-- MySQL dump 10.13  Distrib 8.0.17, for Linux (x86_64)
--
-- Host: localhost    Database: projectdb
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_details` (
  `pizzaid` int(11) NOT NULL,
  `orderid` int(11) NOT NULL,
  KEY `FKh35b1ljeu4440iie9psw8a7yt` (`orderid`),
  KEY `FKmm5d24e3ud71csw56x620jhl9` (`pizzaid`),
  CONSTRAINT `FKh35b1ljeu4440iie9psw8a7yt` FOREIGN KEY (`orderid`) REFERENCES `orders` (`id`),
  CONSTRAINT `FKmm5d24e3ud71csw56x620jhl9` FOREIGN KEY (`pizzaid`) REFERENCES `pizzas` (`pizzaid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_details`
--

LOCK TABLES `order_details` WRITE;
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
INSERT INTO `order_details` VALUES (101,501),(102,501),(103,501),(107,501),(106,501),(108,502),(109,502),(110,504),(101,505),(101,505),(102,505),(101,506),(102,506),(108,507),(106,507);
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `pizaaqty` int(11) NOT NULL,
  `status` varchar(15) DEFAULT NULL,
  `totalamt` float NOT NULL,
  `addrid` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKiok1mkvcnufkno0wahclaf83o` (`addrid`),
  KEY `FK58x4l9shxmkb7pismj4ilt7pj` (`uid`),
  CONSTRAINT `FK58x4l9shxmkb7pismj4ilt7pj` FOREIGN KEY (`uid`) REFERENCES `user` (`id`),
  CONSTRAINT `FKiok1mkvcnufkno0wahclaf83o` FOREIGN KEY (`addrid`) REFERENCES `permanent_address` (`addrid`)
) ENGINE=InnoDB AUTO_INCREMENT=508 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (501,'2019-01-01',5,'Delivered',1500,1,1),(502,'2019-01-02',2,'Delivered',800,2,2),(503,'2019-01-03',4,'Delivered',600,3,3),(504,'2019-01-05',1,'NotDelivered',0,4,4),(505,'2019-01-17',3,'Delivered',680,1,5),(506,'2020-01-20',6,'Delivered',1600,1,1),(507,'2020-01-13',4,'NotDelivered',600,1,1);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permanent_address`
--

DROP TABLE IF EXISTS `permanent_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permanent_address` (
  `addrid` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(30) NOT NULL,
  `flatno` int(11) NOT NULL,
  `pincode` int(11) NOT NULL,
  `street` varchar(30) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`addrid`),
  KEY `FKhsfnyu4rq6nnixwjic0bitgso` (`uid`),
  CONSTRAINT `FKhsfnyu4rq6nnixwjic0bitgso` FOREIGN KEY (`uid`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permanent_address`
--

LOCK TABLES `permanent_address` WRITE;
/*!40000 ALTER TABLE `permanent_address` DISABLE KEYS */;
INSERT INTO `permanent_address` VALUES (1,'ICHAL',420,416115,'SHASTRI',1),(2,'KOP',786,411556,'GANDHI',2),(3,'JAMKHED',416,415520,'NEHARU',3),(4,'KOP',113,416115,'TARARANI',1);
/*!40000 ALTER TABLE `permanent_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pizzas`
--

DROP TABLE IF EXISTS `pizzas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pizzas` (
  `pizzaid` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(15) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `imgpath` varchar(255) DEFAULT NULL,
  `pname` varchar(60) DEFAULT NULL,
  `price` float NOT NULL,
  `size` varchar(15) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`pizzaid`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pizzas`
--

LOCK TABLES `pizzas` WRITE;
/*!40000 ALTER TABLE `pizzas` DISABLE KEYS */;
INSERT INTO `pizzas` VALUES (101,'VEG','Baby Corn, Black Olives, Green Capsicum, Jalapeno, Red Capsicum','/assets/veg_pizzas/Corn_&_Cheese.jpg','Veg Exotica',100,'PERSONAL','AVAILABLE'),(102,'VEG','Green Capsicum, Masala Paneer, Masala Soya Chunk, Onion, Red Paprika','/assets/veg_pizzas/Deluxe_Veggie.jpg','Paneer Soya Supreme',200,'FAMILY','AVAILABLE'),(103,'VEG','Black Olives, Green And Yellow Zucchini, Jalapeno, Mushroom, Red Capsicum','/assets/veg_pizzas/Digital_Veggie_Paradise_olo_266x265.jpg','Veggie Italiano',300,'MEDIUM','AVAILABLE'),(104,'VEG','Black Olives, Green Capsicum, Mushroom, Onion, Red Paprika, Sweet Corn','/assets/veg_pizzas/Double_Cheese_Margherita.jpg','Veggie Supreme',400,'PERSONAL','AVAILABLE'),(105,'NONVEG','Chicken Pepperoni, Green And Yellow Zucchini, Jalapeno, Smoked Chicken','/assets/non_veg_pizzas/IndianChickenTikka.jpg','Chicken Exotica',500,'FAMILY','NOTAVAILABLE'),(106,'NONVEG','Black Olives, Chicken Pepperoni, Chicken Sausage, Green Capsicum, Mushroom','/assets/non_veg_pizzas/Non-Veg_Supreme.jpg','Chicken Italiano',250,'MEDIUM','NOTAVAILABLE'),(107,'NONVEG','Chicken Tikka, Lebanese Chicken, Schezwan Chicken Meat Ball','/assets/non_veg_pizzas/Pepper_Barbeque.jpg','Chicken Supreme',350,'PERSONAL','AVAILABLE'),(108,'NONVEG','Chicken Sausage, Green Capsicum, Lebanese Chicken, Onion, Red Paprika, Schezwan Chicken Meat Ball','/assets/non_veg_pizzas/Pepper_Barbeque_&_Onion.jpg','Triple Chicken Feast',450,'FAMILY','NOTAVAILABLE'),(109,'VEG','Cheese','/assets/veg_pizzas/Corn_&_Cheese.jpg','Margherita',550,'MEDIUM','NOTAVAILABLE'),(110,'NONVEG','Chicken Sausage, Onion','/assets/veg_pizzas/Corn_&_Cheese.jpg','Chicken Sausage',150,'PERSONAL','NOTAVAILABLE'),(111,'VEG','Green Capsicum, Masala Paneer, Masala Soya Chunk, Onion, Red Paprika','/assets/veg_pizzas/Deluxe_Veggie.jpg','Paneer PizzaHot Special',600,'FAMILY','AVAILABLE'),(113,'NONVEG','Green Capsicum, Masala Paneer, Masala Soya Chunk, Onion, Red Paprika','/assets/veg_pizzas/Deluxe_Veggie.jpg','vishal Special',100,'FAMILY','AVAILABLE');
/*!40000 ALTER TABLE `pizzas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dob` date DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `mobNo` varchar(10) DEFAULT NULL,
  `pass` varchar(60) DEFAULT NULL,
  `role` varchar(15) DEFAULT NULL,
  `uname` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKgq8i8bjk7osiatf3ha4c1789u` (`email`,`mobNo`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'1995-01-17','vishal@gmail.com','7843052772','zxcvbnm','VENDOR','vishalm'),(2,'1995-01-17','akash@gmail.com','7843052771','zxcvbnm','USER','Akashw'),(3,'1997-01-07','Ashish@gmail.com','7843052773','zxcvbnm','USER','AshishR'),(4,'1995-01-17','Ajinkya@gmail.com','9960735906','zxcvbnm','USER','Ajinkya'),(5,'1995-01-17','Aniket@gmail.com','9960735906','zxcvbnm','USER','Aniket'),(14,'1993-10-26','vishu@gmail.com','7843052772','zxcvbnm','USER','vishum');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-20  2:39:57
