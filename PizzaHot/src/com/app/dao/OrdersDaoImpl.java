package com.app.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.NewOrder;
import com.app.pojos.OrderStatus;
import com.app.pojos.Orders;


@Repository
public class OrdersDaoImpl implements IOrdersDao {

	@Autowired
	private SessionFactory sf;
	
	@Autowired
	private IUserDao Udao;
	
	@Autowired
	private IAddressDao Adao;
	
	@Autowired
	private IPizzaDao Pdao;
	
	@Override
	public List<Orders> getAllOrders() {
		String jpql = "select o from Orders o ORDER BY o.date DESC";
		//SELECT a FROM Author a ORDER BY a.lastName ASC, a.firstName DESC
		List<Orders> orderslist = sf.getCurrentSession().createQuery(jpql, Orders.class).getResultList();
		return orderslist;
	}

	@Override
	public Orders getOrdersDetails(int oid) {
		String jpql = "select o from Orders o where o.id = :oid";
		return sf.getCurrentSession().createQuery(jpql, Orders.class).setParameter("oid", oid).getSingleResult();
	}

	@Override
	public int addOrders(NewOrder[] newOrders, int uid) {
		Orders order = new Orders();  
	    Date date = new Date();
	    order.setDate(date);
	    order.setStatus(OrderStatus.NotDelivered);
	    order.setTotalamt(newOrders[0].totalamt);
		order.setPizaaqty(newOrders[0].pizzaqty);
		order.setOaddr(Adao.getAddressDetails(newOrders[0].addrid));
		order.setuser(Udao.getUserDetails(uid));
		for (int i = 0; i < newOrders.length; i++) {
			order.addPizzas(Pdao.getPizzaDetails(newOrders[i].pizzaid));
		}
		sf.getCurrentSession().save(order);
		return 1;
	}

	@Override
	public String deleteOrders(int oid) {
		sf.getCurrentSession().delete(getOrdersDetails(oid));
		return "Deleted Orders info...!!";
	}

	@Override
	public String updateOrders(Orders orders) {
		Session hs=sf.getCurrentSession();
		hs.clear();
		hs.update(orders);
		return "Orders details updated....";		 
	}
	
	@Override
	public int updateOrderStatusDelivered(int oid)
	{
		Orders order = sf.getCurrentSession().get(Orders.class, oid);
		order.setStatus(OrderStatus.Delivered);
		sf.getCurrentSession().update(order);
		return 1;
	}
	
	@Override
	public int updateOrderStatusCanceled(int oid)
	{
		Orders order = sf.getCurrentSession().get(Orders.class, oid);
		order.setStatus(OrderStatus.Canceled);
		sf.getCurrentSession().update(order);
		return 1;
	}

}
