package com.app.dao;

import java.util.List;

import com.app.pojos.NewOrder;
import com.app.pojos.Orders;

public interface IOrdersDao {
	List<Orders> getAllOrders();
	Orders getOrdersDetails(int oid);
	int addOrders(NewOrder[] newOrder, int uid);
	String deleteOrders(int oid);
	String updateOrders(Orders orders);
	int updateOrderStatusDelivered(int oid);
	int updateOrderStatusCanceled(int oid);
}
