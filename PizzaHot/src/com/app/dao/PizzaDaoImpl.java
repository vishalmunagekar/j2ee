	package com.app.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.OrderStatus;
import com.app.pojos.Orders;
import com.app.pojos.PizzaAvailabilityStatus;
import com.app.pojos.PizzaCategory;
import com.app.pojos.PizzaSize;
import com.app.pojos.Pizzas;


@Repository
public class PizzaDaoImpl implements IPizzaDao {

	@Autowired
	private SessionFactory sf;
	
	@Override
	public List<Pizzas> getAllPizzas() {
		String jpql = "select p from Pizzas p";
		List<Pizzas> pizzalist = sf.getCurrentSession().createQuery(jpql, Pizzas.class).getResultList();
		return pizzalist;
	}

	@Override
	public Pizzas getPizzaDetails(int pid) {
		String jpql = "select p from Pizzas p where p.pizzaid = :pid";
		return sf.getCurrentSession().createQuery(jpql, Pizzas.class).setParameter("pid", pid).getSingleResult();
	}

	@Override
	public int addPizza(Pizzas pizza) {
		pizza.setStatus(PizzaAvailabilityStatus.AVAILABLE);		
		System.out.println(pizza);
		sf.getCurrentSession().persist(pizza);
		return 1;
	}

	@Override
	public String deletePizza(int pid) {
		sf.getCurrentSession().delete(getPizzaDetails(pid));
		return "Deleted Pizza info...!!";
	}

	@Override
	public String updatePizza(Pizzas pizza) {
		Session hs=sf.getCurrentSession();
		hs.clear();
		hs.update(pizza);
		return "Pizza details updated...."; 
	}

	@Override
	public List<Pizzas> getVegPizzas() {
		String jpql = "select p from Pizzas p where p.category = :VEG";
		List<Pizzas> vegPizzas = sf.getCurrentSession().createQuery(jpql, Pizzas.class).setParameter("VEG", PizzaCategory.VEG).getResultList();
		return vegPizzas;
	}

	@Override
	public List<Pizzas> getNonVegPizzas() {
		String jpql = "select p from Pizzas p where p.category = :NONVEG";
		List<Pizzas> nonVegPizzas = sf.getCurrentSession().createQuery(jpql, Pizzas.class).setParameter("NONVEG", PizzaCategory.NONVEG).getResultList();
		return nonVegPizzas;
	}

	@Override
	public List<Pizzas> getAvailablePizzas() {
		String jpql = "select p from Pizzas p where p.status = :AVAILABLE";
		List<Pizzas> AvailablePizzas = sf.getCurrentSession().createQuery(jpql, Pizzas.class).setParameter("AVAILABLE",PizzaAvailabilityStatus.AVAILABLE).getResultList();
		return AvailablePizzas;
	}

	@Override
	public List<Pizzas> getNotAvailablePizzas() {
		String jpql = "select p from Pizzas p where p.status = :NOTAVAILABLE";
		List<Pizzas> NotAvailablePizzas = sf.getCurrentSession().createQuery(jpql, Pizzas.class).setParameter("NOTAVAILABLE", PizzaAvailabilityStatus.NOTAVAILABLE).getResultList();
		return NotAvailablePizzas;
	}

	@Override
	public List<Pizzas> getBySize(String size) {
		String jpql = "select p from Pizzas p where p.size = :size";
		List<Pizzas> Pizzas = sf.getCurrentSession().createQuery(jpql, Pizzas.class).setParameter("size", PizzaSize.valueOf(size.toUpperCase())).getResultList();
		return Pizzas;
	}

	@Override
	public List<Pizzas> getPizzaByName(String name) {
		String jpql = "select p from Pizzas p where p.pname = :name";
		return sf.getCurrentSession().createQuery(jpql, Pizzas.class).setParameter("name", name.toUpperCase()).getResultList();
		
	}

	@Override
	public int changeStatusAvailable(int pid) {
		Pizzas pizza = sf.getCurrentSession().get(Pizzas.class, pid);
		pizza.setStatus(PizzaAvailabilityStatus.AVAILABLE);
		sf.getCurrentSession().update(pizza);
		System.out.println(pizza);
		return 1;
	}

	@Override
	public int changeStatusNotavailable(int pid) {
		Pizzas pizza = sf.getCurrentSession().get(Pizzas.class, pid);
		pizza.setStatus(PizzaAvailabilityStatus.NOTAVAILABLE);
		sf.getCurrentSession().update(pizza);
		System.out.println(pizza);
		return 1;
	}

}



/*
 * * Pizza p = getPizzaDetails(pizza.getPizzaid()); p.setPname(pizza.getPname());
 * p.setCategory(pizza.getCategory()); p.setImgpath(pizza.getImgpath());
 * p.setDescription(pizza.getDescription()); p.setStatus(pizza.getStatus());
 * p.setSize(pizza.getSize()); p.setPrice(pizza.getPrice()); hs.update(p);
 */
