package com.app.dao;

import java.util.List;

import com.app.pojos.Pizzas;

public interface IPizzaDao {
	List<Pizzas> getAllPizzas();
	Pizzas getPizzaDetails(int pid);
	int addPizza(Pizzas pizza);
	String deletePizza(int pid);
	String updatePizza(Pizzas pizza);
	List<Pizzas> getVegPizzas();
	List<Pizzas> getNonVegPizzas();
	List<Pizzas> getAvailablePizzas(); //AVAILABLE 
	List<Pizzas> getNotAvailablePizzas(); //NOTAVAILABLE
	List<Pizzas> getBySize(String size); //PERSONAL, MEDIUM, FAMILY
	List<Pizzas> getPizzaByName(String Name);
	int changeStatusAvailable(int pid);
	int changeStatusNotavailable(int pid);
}	
