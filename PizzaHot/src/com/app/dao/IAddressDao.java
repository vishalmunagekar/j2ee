package com.app.dao;

import java.util.List;

import com.app.pojos.Address;

public interface IAddressDao {
	List<Address> getAllAddress();
	Address getAddressDetails(int aid);
	int addAddress(Address address, int uid);
	String deleteAddress(int aid);
	String updateAddress(Address address);
}	