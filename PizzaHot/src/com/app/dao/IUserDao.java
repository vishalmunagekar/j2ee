package com.app.dao;

import java.util.List;

import com.app.pojos.User;

public interface IUserDao {
	List<User> getAllUsers();
	User getUserDetails(int id);
	int updateUserDetails(User u);
	String deleteUser(int id);
	User authentication(User u);
	int addUserDetails(User u);
	User getUserByEmail(String email);
	int forgotUserPassword(String email);
}
