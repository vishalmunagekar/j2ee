package com.app.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Address;
import com.app.pojos.User;

@Repository
public class AddressDaoImpl implements IAddressDao {

	@Autowired
	private SessionFactory sf;
	
	@Override
	public List<Address> getAllAddress() {
		String jpql = "select a from Address a";
		return sf.getCurrentSession().createQuery(jpql, Address.class).getResultList();
	}

	@Override
	public Address getAddressDetails(int aid) {
		String jpql = "select a from Address a where a.addrid = :aid";
		return sf.getCurrentSession().createQuery(jpql, Address.class).setParameter("aid", aid).getSingleResult();
	}

	@Override
	public int addAddress(Address address, int uid) {
		User u = sf.getCurrentSession().get(User.class, uid);
		u.addAddress(address);
		sf.getCurrentSession().persist(u);
		return 1;
	}

	@Override
	public String deleteAddress(int aid) {
		sf.getCurrentSession().delete(getAddressDetails(aid));
		return "Deleted Orders info...!!";
	}

	@Override
	public String updateAddress(Address address) {
		Session hs=sf.getCurrentSession();
		hs.clear();
		hs.update(address);
		return "Address details updated....";
	}

}
