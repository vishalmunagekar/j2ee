package com.app.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.pojos.User;


@Repository
@Transactional
public class UserDaoImpl implements IUserDao {

	@Autowired
	private SessionFactory sf;

	@Autowired
	private JavaMailSender mailSender;

	@Override
	public List<User> getAllUsers() {
		String jpql = "select u from User u";
		return sf.getCurrentSession().createQuery(jpql, User.class).getResultList();
	}

	@Override
	public User getUserDetails(int uid) {
		String jpql = "select u from User u where u.id = :id";
		try {
			User user = sf.getCurrentSession().createQuery(jpql, User.class).setParameter("id", uid).getSingleResult();
			return user;
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public User getUserByEmail(String email) {
		String jpql = "select u from User u where u.email = :em";
		try {
			User user = sf.getCurrentSession().createQuery(jpql, User.class).setParameter("em", email).getSingleResult();
			return user;
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public int updateUserDetails(User u)
	{
		Session hs=sf.getCurrentSession();
		hs.get(User.class, u.getId());
		hs.clear();
		hs.update(u);
		return 1;
	}

	@Override
	public int addUserDetails(User u)
	{
		sf.getCurrentSession().persist(u);
		String msg="Succsessfully Registered to PizzaHot India Private Ltd. Your details "+u.toString();
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(u.getEmail());
		mailMessage.setSubject("Registration on PizzaHot India Private Ltd.");
		mailMessage.setText(msg);
		try
		{
			mailSender.send(mailMessage);
		}
		catch (MailException e) 
		{
			System.out.println("inside mail exception");
			e.printStackTrace();
		}
		return u.getId();
	}

	@Override
	public String deleteUser(int id) {
		sf.getCurrentSession().delete(getUserDetails(id));
		return "Deleted User info";
	}

	@Override
	public User authentication(User u) {

		Session hs = sf.getCurrentSession();
		System.out.println(u);
		String jpql = "select u from User u left outer join fetch u.orders where u.email = :email and u.pass = :pass";
		try {
			User user = hs.createQuery(jpql, User.class).setParameter("email", u.getEmail()).setParameter("pass", u.getPass()).getSingleResult();
			return user;
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public int forgotUserPassword(String email) {
		User user = getUserByEmail(email);
		user.setPass(user.getMobNo());

		String msg="Password has been changed for PizzaHot India Private Ltd."+
				"New Password is "+user.getPass()+"( Note - After Login Please Change the Password";
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(user.getEmail());
		mailMessage.setSubject("Password Changed for PizzaHot India Private Ltd.");
		mailMessage.setText(msg);
		try
		{
			mailSender.send(mailMessage);
		}
		catch (MailException e) 
		{
			System.out.println("inside mail exception");
			e.printStackTrace();
			return 0;
		}
		return 1;
	}
}















//select u from User u left outer join fetch u.orders where u.id = :id

/*
try
{
	ufounded = hs.createQuery(jpql, User.class).setParameter("email", u.getEmail()).setParameter("pass", u.getPass()).getSingleResult();
}
catch (NoResultException ex)
{
	ex.printStackTrace();
}
if(ufounded == null){
	return null;	
}
else {
	return ufounded;
}
 */
//SELECT b FROM Book b JOIN FETCH b.authors a JOIN FETCH b.reviews r WHERE b.id = 1
