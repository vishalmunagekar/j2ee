package com.app.service;

import java.util.List;

import com.app.pojos.Address;
import com.app.pojos.User;

public interface IUserService {
	List<User> getAllUsers();
	User getUserDetails(int id);
	int updateUserDetails(User u);
	String deleteUser(int id);
	int addUserDetails(User u);
	User authentication(User u);
	User getUserByEmail(String email);
	int forgotUserPassword(String email);
	int addAddress(Address address, int uid);
}
