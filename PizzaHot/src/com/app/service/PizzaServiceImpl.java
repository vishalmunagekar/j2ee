package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.IPizzaDao;
import com.app.pojos.Pizzas;


@Service
@Transactional
public class PizzaServiceImpl implements IPizzaService {

	@Autowired
	private IPizzaDao Pdao;
	
	@Override
	public List<Pizzas> getAllPizzas() {
		return Pdao.getAllPizzas();
	}

	@Override
	public Pizzas getPizzaDetails(int pid) {
		return Pdao.getPizzaDetails(pid);
	}

	@Override
	public int addPizza(Pizzas pizza) {
		return Pdao.addPizza(pizza);
	}

	@Override
	public String deletePizza(int pid) {
		return Pdao.deletePizza(pid);
	}

	@Override
	public String updatePizza(Pizzas pizza) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Pizzas> getVegPizzas() {
		return Pdao.getVegPizzas();
	}

	@Override
	public List<Pizzas> getNonVegPizzas() {
		return Pdao.getNonVegPizzas();
	}

	@Override
	public List<Pizzas> getAvailablePizzas() {
		return Pdao.getAvailablePizzas();
	}

	@Override
	public List<Pizzas> getNotAvailablePizzas() {
		return Pdao.getNotAvailablePizzas();
	}

	@Override
	public List<Pizzas> getBySize(String size) {
		return Pdao.getBySize(size);
	}

	@Override
	public List<Pizzas> getPizzaByName(String name) {
		return Pdao.getPizzaByName(name);
	}

	@Override
	public int changeStatusAvailable(int pid) {
		return Pdao.changeStatusAvailable(pid);
	}

	@Override
	public int changeStatusNotavailable(int pid) {
		return Pdao.changeStatusNotavailable(pid);
	}

}
