package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.IOrdersDao;
import com.app.pojos.NewOrder;
import com.app.pojos.Orders;


@Service
@Transactional
public class OrdersServiceImpl implements IOrdersService{

	@Autowired
	private IOrdersDao Odao;
	
	@Override
	public List<Orders> getAllOrders() {
		return Odao.getAllOrders();
	}

	@Override
	public Orders getOrdersDetails(int oid) {
		return Odao.getOrdersDetails(oid);
	}

	@Override
	public int addOrders(NewOrder[] newOrder, int uid) {
		return Odao.addOrders(newOrder, uid);
	}

	@Override
	public String deleteOrders(int oid) {
		return Odao.deleteOrders(oid);
	}

	@Override
	public String updateOrders(Orders orders) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int updateOrderStatusDelivered(int oid) {
		return Odao.updateOrderStatusDelivered(oid);
	}
	
	@Override
	public int updateOrderStatusCanceled(int oid) {
		return Odao.updateOrderStatusCanceled(oid);
	}

}
