package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.IAddressDao;
import com.app.dao.IUserDao;
import com.app.pojos.Address;
import com.app.pojos.User;

@Service
@Transactional
public class UserServiceImpl implements IUserService {

	@Autowired
	private IUserDao Udao;
	
	@Autowired
	private IAddressDao Adao;
	
	@Override
	public List<User> getAllUsers() {
		return Udao.getAllUsers();
	}

	@Override
	public User getUserDetails(int id) {
		return Udao.getUserDetails(id);
	}

	@Override
	public int updateUserDetails(User u) {
		return Udao.updateUserDetails(u);
	}

	@Override
	public String deleteUser(int id) {
		Udao.deleteUser(id);
		return "User Deleted...";
	}

	@Override
	public User authentication(User u) {
		return Udao.authentication(u);
	}

	@Override
	public int addUserDetails(User u) {
		return Udao.addUserDetails(u);
	}

	@Override
	public User getUserByEmail(String email) {
		return Udao.getUserByEmail(email);
	}

	@Override
	public int forgotUserPassword(String email) {
		return Udao.forgotUserPassword(email);
	}

	@Override
	public int addAddress(Address address, int uid) {
		return Adao.addAddress(address, uid);
	}
	
	

}
