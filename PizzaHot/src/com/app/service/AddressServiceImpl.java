package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.IAddressDao;
import com.app.pojos.Address;
import com.app.pojos.User;

@Service
@Transactional
public class AddressServiceImpl implements IAddressService {

	@Autowired
	private IAddressDao Adao; 
	
	@Override
	public List<Address> getAllAddress() {
		return Adao.getAllAddress();
	}

	@Override
	public Address getAddressDetails(int aid) {
		return Adao.getAddressDetails(aid);
	}

	@Override
	public int addAddress(Address address, int uid) {
		return Adao.addAddress(address, uid);
	}

	@Override
	public String deleteAddress(int aid) {
		return Adao.deleteAddress(aid);
	}

	@Override
	public String updateAddress(Address address) {
		return Adao.updateAddress(address);
	}
}
