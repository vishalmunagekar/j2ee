package com.app.service;

import java.util.List;

import com.app.pojos.Address;
import com.app.pojos.User;

public interface IAddressService {
	List<Address> getAllAddress();
	Address getAddressDetails(int aid);
	int addAddress(Address address, int uid);
	String deleteAddress(int aid);
	String updateAddress(Address address);
}
