package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Address;
import com.app.pojos.User;
import com.app.pojos.UserRole;
import com.app.service.IUserService;

@RestController
@RequestMapping("/User")
@CrossOrigin
public class UserController {
	
	@Autowired
	private IUserService service;

	@GetMapping
	public List<User> getAllUsers() {
		return service.getAllUsers();
	}

	@GetMapping("/{id}")
	public User getUserDetails(@PathVariable int id) {
		return service.getUserDetails(id);
	}

	@PutMapping("/update")
	public int updateUserDetails(@RequestBody User u) {
		System.out.println(u);
		return service.updateUserDetails(u);
	}

	@DeleteMapping("/delete/{id}")
	public String deleteUser(@PathVariable int id) {
		return service.deleteUser(id);
	}
	
	@PostMapping("/signup")
	public int addUserDetails(@RequestBody User u)
	{
		u.setRole(UserRole.valueOf("USER"));
		return service.addUserDetails(u);
	}

	@PostMapping("/login")
	public User authentication(@RequestBody User u) {
		//System.out.println(u);
		return service.authentication(u);
	}
	
	@PostMapping("/userbyemail")
	public User getUserByEmail(@RequestBody String email) {
		return service.getUserByEmail(email);
	}

	@PostMapping("/forgotpass")
	public int forgotUserPassword(@RequestBody String email) {
		System.out.println(email);
		return service.forgotUserPassword(email);
	}
	
	@PostMapping("/addaddress/{uid}")
	public int addAddress(@PathVariable int uid, @RequestBody Address address)
	{
		return service.addAddress(address, uid);
	}
	
}