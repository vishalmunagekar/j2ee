package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Address;
import com.app.service.IAddressService;

@RestController
@RequestMapping("/Address")
@CrossOrigin
public class AddressController {
	
	@Autowired
	private IAddressService service;

	@GetMapping
	public List<Address> getAllAddress() {
		return service.getAllAddress();
	}

	@GetMapping("/{aid}")
	public Address getAddressDetails(@PathVariable int aid) {
		return service.getAddressDetails(aid);
	}

//	@PostMapping("/add")
//	public String addAddress(@RequestBody Address address) {
//		return service.addAddress(address, null);
//	}

	@DeleteMapping("/delete/{oid}")
	public String deleteAddress(@PathVariable int aid) {
		return service.deleteAddress(aid);
	}

	@PutMapping("/update")
	public String updateAddress(@RequestBody Address address) {
		return service.updateAddress(address);
	}
}
