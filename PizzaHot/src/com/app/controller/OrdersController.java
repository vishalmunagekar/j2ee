package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.NewOrder;
import com.app.pojos.Orders;
import com.app.service.IOrdersService;

@RestController
@RequestMapping("/Orders")
@CrossOrigin
public class OrdersController {
	
	@Autowired
	private IOrdersService service;

	@GetMapping
	public List<Orders> getAllOrders() {
		return service.getAllOrders();
	}

	@GetMapping("/{oid}")
	public Orders getOrdersDetails(@PathVariable int oid) {
		return service.getOrdersDetails(oid);
	}

	@PostMapping("/add/{uid}")
	public int addOrders(@RequestBody NewOrder[] newOrder, @PathVariable int uid ) {
		for (NewOrder newOrder2 : newOrder) {
			System.out.println(newOrder2);
		}
		return service.addOrders(newOrder , uid);
	}

	@DeleteMapping("/delete/{oid}")
	public String deleteOrders(@PathVariable int oid) {
		return service.deleteOrders(oid);
	}

//	@PutMapping("/update")
//	public String updateOrders(@RequestBody Orders orders) {
//		return service.updateOrders(orders);
//	}
	
	@GetMapping("/delivered/{oid}")
	public int updateOrderStatusDelivered(@PathVariable int oid)
	{
		return service.updateOrderStatusDelivered(oid);
	}
	
	@GetMapping("/canceled/{oid}")
	public int updateOrderStatusCanceled(@PathVariable int oid)
	{
		return service.updateOrderStatusCanceled(oid);
	}
}
