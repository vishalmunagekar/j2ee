package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.app.pojos.PizzaAvailabilityStatus;
import com.app.pojos.PizzaCategory;
import com.app.pojos.PizzaSize;
import com.app.pojos.Pizzas;
import com.app.service.IPizzaService;

@RestController
@RequestMapping("/Pizza")
@CrossOrigin
public class PizzaController {
	
	@Autowired
	private IPizzaService service;

	@GetMapping
	public List<Pizzas> getAllPizzas() {
		return service.getAllPizzas();
	}

	@GetMapping("/{pid}")
	public Pizzas getPizzaDetails(@PathVariable int pid) {
		return service.getPizzaDetails(pid);
	}

	@PostMapping("/add")
	public int addPizza(@RequestParam String pname, @RequestParam String category,
			@RequestParam(value = "image", required = false) MultipartFile image,
			@RequestParam String description,@RequestParam String size,
			@RequestParam float price) {
		Pizzas pizza = new Pizzas();
		pizza.setPname(pname); pizza.setCategory(PizzaCategory.valueOf(category));
		pizza.setDescription(description);pizza.setSize(PizzaSize.valueOf(size));
		pizza.setPrice(price);
		
		try {
			pizza.setImage(image.getBytes());
			System.out.println(pizza);
			service.addPizza(pizza);
		} catch (Exception e) {
			return 0;
		}		
		return 1;
	}

	@DeleteMapping("/delete/{pid}")
	public String deletePizza(@PathVariable int pid) {
		return service.deletePizza(pid);
	}

	@PutMapping("/update")
	public String updatePizza(@RequestBody Pizzas pizza) {
		return service.updatePizza(pizza);
	}
	
	@GetMapping("/Veg")
	public List<Pizzas> getVegPizzas() {
		return service.getVegPizzas();
	}
	
	@GetMapping("/NonVeg")
	public List<Pizzas> getNonVegPizzas() {
		return service.getNonVegPizzas();
	}

	@GetMapping("/BySize/{size}")
	public List<Pizzas> getBySize(@PathVariable String size) {
		return service.getBySize(size);
	}

	@GetMapping("/ByName/{name}")
	public List<Pizzas> getPizzaByName(@PathVariable String name) {
		return service.getPizzaByName(name);
	}
	
	@GetMapping("/Available/{pid}")
	public int changeStatusAvailable(@PathVariable int pid) {
		return service.changeStatusAvailable(pid);
	}

	@GetMapping("/Notavailable/{pid}")
	public int changeStatusNotavailable(@PathVariable int pid) {
		return service.changeStatusNotavailable(pid);
	}
	
//	@GetMapping("/Available")
//	public List<Pizzas> getAvailablePizzas() {
//		return service.getAvailablePizzas();
//	}
//	
//	@GetMapping("NotAvailable")
//	public List<Pizzas> getNotAvailablePizzas() {
//		return service.getNotAvailablePizzas();
//	}
}
