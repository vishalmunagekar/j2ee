package com.app.pojos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "user", uniqueConstraints={@UniqueConstraint(columnNames = {"email", "mobNo"})})
public class User {
	private Integer id;
	private String uname;
	private String mobNo;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dob;
	private String email;
	private String pass;
	private List<Address> addresses = new ArrayList<Address>();
	private UserRole role; //VENDOR, USER;
	private  List<Orders> orders = new ArrayList<>();
	
	public User() {
		System.out.println("User() POJO Works...");
	}

	public User(Integer id, String uname, String mobNo, Date dob, String email, String pass, UserRole role) {
		super();
		this.id = id;
		this.uname = uname;
		this.mobNo = mobNo;
		this.dob = dob;
		this.email = email;
		this.pass = pass;
		this.role = role;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(length = 60)
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	
	@Column(length = 10)
	public String getMobNo() {
		return mobNo;
	}
	public void setMobNo(String mobNo) {
		this.mobNo = mobNo;
	}
	
	@Temporal(TemporalType.DATE)
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	
	@Column(length = 60)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(length = 60)
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval=true,  fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	public List<Address> getAddresses() {
		return addresses;
	}
	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	@Column(length = 15)
	@Enumerated(EnumType.STRING)
	public UserRole getRole() {
		return role;
	}
	public void setRole(UserRole role) {
		this.role = role;
	}

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval=true, fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	public List<Orders> getOrders() {
		return orders;
	}
	public void setOrders(List<Orders> orders) {
		this.orders = orders;
	}
	
	public void addAddress(Address address)
	{
		address.setUser(this);
		addresses.add(address);
		
	}
	
	public void removeAddress(Address address) {
		address.setUser(null);
		addresses.remove(address);
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", uname=" + uname + ", mobNo=" + mobNo + ", dob=" + dob + ", email=" + email
				+ ", pass=" + pass + ", role=" + role + "]";
	}
}
