package com.app.pojos;

public enum PizzaAvailabilityStatus {
	AVAILABLE, NOTAVAILABLE;
}
