package com.app.pojos;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "pizzas")
public class Pizzas {
	private Integer pizzaid;
	private String pname;
	private PizzaCategory category; //VEG, NONVEG;
	private byte[] image;
	private String description;
	private PizzaAvailabilityStatus status; //AVAILABLE, NOTAVAILABLE
	private PizzaSize size; //PERSONAL, MEDIUM, FAMILY
	private float price;
	@JsonBackReference
	public List<Orders> orders = new ArrayList<Orders>();

	public Pizzas() {
		System.out.println("Pizzas() POJO works...");
	}

	public Pizzas(Integer pizzaid, String pname, PizzaCategory category, String description,
			PizzaAvailabilityStatus status, PizzaSize size, float price){
		super();
		this.pizzaid = pizzaid;
		this.pname = pname;
		this.category = category;
		this.description = description;
		this.status = status;
		this.size = size;
		this.price = price;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getPizzaid() {
		return pizzaid;
	}
	public void setPizzaid(Integer pizzaid) {
		this.pizzaid = pizzaid;
	}

	@Column(length = 60)
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}

	@Column(length = 15)
	@Enumerated(EnumType.STRING)
	public PizzaCategory getCategory() {
		return category;
	}
	public void setCategory(PizzaCategory category) {
		this.category = category;
	}

	@Column(length = 15)
	@Enumerated(EnumType.STRING)
	public PizzaAvailabilityStatus getStatus() {
		return status;
	}
	public void setStatus(PizzaAvailabilityStatus status) {
		this.status = status;
	}

	@Column(length = 15)
	@Enumerated(EnumType.STRING)
	public PizzaSize getSize() {
		return size;
	}
	public void setSize(PizzaSize size) {
		this.size = size;
	}

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	@JoinTable(name = "order_details", joinColumns = @JoinColumn(name = "pizzaid"), inverseJoinColumns = @JoinColumn(name = "orderid"))
	public List<Orders> getOrders() {
		return orders;
	}
	public void setOrders(List<Orders> orders) {
		this.orders = orders;
	}

	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}

	@Lob
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return "Pizzas [pizzaid=" + pizzaid + ", pname=" + pname + ", category=" + category + ", image=" + image
				+ ", description=" + description + ", status=" + status + ", size=" + size + ", price=" + price + "]";
	}
}
