package com.app.pojos;

public class NewOrder {
	public int pizzaid;
	public int pizzaqty;
	public float totalamt;
	public int addrid;
	
	public NewOrder() {
		System.out.println("NewOrder()");
	}

	public NewOrder(int pizzaid, int pizzaqty, float totalamt, int addrid) {
		super();
		this.pizzaid = pizzaid;
		this.pizzaqty = pizzaqty;
		this.totalamt = totalamt;
		this.addrid = addrid;
	}

	public int getPizzaid() {
		return pizzaid;
	}

	public void setPizzaid(int pizzaid) {
		this.pizzaid = pizzaid;
	}

	public int getPizzaqty() {
		return pizzaqty;
	}

	public void setPizzaqty(int pizzaqty) {
		this.pizzaqty = pizzaqty;
	}

	public float getTotalamt() {
		return totalamt;
	}

	public void setTotalamt(float totalamt) {
		this.totalamt = totalamt;
	}

	public int getAddrid() {
		return addrid;
	}

	public void setAddrid(int addrid) {
		this.addrid = addrid;
	}

	@Override
	public String toString() {
		return "NewOrder [pizzaid=" + pizzaid + ", pizzaqty=" + pizzaqty + ", totalamt=" + totalamt + ", addrid="
				+ addrid + "]";
	}
}
