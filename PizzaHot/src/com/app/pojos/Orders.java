package com.app.pojos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "orders")
public class Orders {
	private Integer id;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date date;
	private float totalamt;
	private OrderStatus status; //Delivered, NotDelivered;
	//@JsonBackReference
	public List<Pizzas> pizzas = new ArrayList<Pizzas>();
	private int pizaaqty;
	@JsonIgnore
	private User user;
	private Address oaddr;

	public Orders() {
		System.out.println("Orders() POJO works...");
	}

	public Orders(Integer id, Date date, float totalamt, OrderStatus status, int pizaaqty, User user,
			Address oaddr) {
		super();
		this.id = id;
		this.date = date;
		this.totalamt = totalamt;
		this.status = status;
		this.pizaaqty = pizaaqty;
		this.user = user;
		this.oaddr = oaddr;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(length = 15)
	@Enumerated(EnumType.STRING)
	public OrderStatus getStatus() {
		return status;
	}
	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	@Temporal(TemporalType.DATE)
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	public float getTotalamt() {
		return totalamt;
	}
	public void setTotalamt(float totalamt) {
		this.totalamt = totalamt;
	}

	@ManyToMany(mappedBy = "orders",fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SUBSELECT)
	public List<Pizzas> getPizzas() {
		return pizzas;
	}
	public void setPizzas(List<Pizzas> pizzas) {
		this.pizzas = pizzas;
	}

	public int getPizaaqty() {
		return pizaaqty;
	}
	public void setPizaaqty(int pizaaqty) {
		this.pizaaqty = pizaaqty;
	}

	@ManyToOne
	@JoinColumn(name="uid")
	public User getuser() {
		return user;
	}
	public void setuser(User user) {
		this.user = user;
	}

	@OneToOne
	@JoinColumn(name="addrid")
	public Address getOaddr() {
		return oaddr;
	}
	public void setOaddr(Address oaddr) {
		this.oaddr = oaddr;
	}
	
	// Convenience method for adding pizzas
	public void addPizzas(Pizzas pizza) {
		this.pizzas.add(pizza);
		pizza.orders.add(this);
	}

}
